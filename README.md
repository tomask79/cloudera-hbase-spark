# Accessing HBase table from Spark2 on the Cloudera platform #

Recently I was assigned on the bigdata project running [HBase](https://hbase.apache.org/) database on the [Cloudera-CDH](https://www.cloudera.com/products/open-source/apache-hadoop/key-cdh-components.html) platform. Until now I've got experience with [Sandbox](https://hortonworks.com/tutorial/hortonworks-sandbox-guide/)
only so I needed to get quickly familiar with Cloudera platform and HBase querying. Hence let me write down few notes about:

* Howto spinnup [cloudera-quickstart](https://www.cloudera.com/downloads/quickstart_vms/5-13.html) VM and install Java 8 and Spark2. By default cloudera-quickstart comes with Spark 1.6 and Java 1.7 
* Howto access HBase from the [Spark2](https://spark.apache.org/releases/spark-release-2-0-0.html) job written in Scala

## Cloudera quickstart VM and Cloudera Manager ##

* Simply download the image from https://www.cloudera.com/downloads/quickstart_vms/5-13.html, I went with VirtualBox platform
* And import the image into VirtualBox. Give it **at least 10GB of memory and 2 CPU cores** otherwise you won't be able to start Cloudera Manager
* About howto launch Cloudera Manager following this [tutorial](https://www.youtube.com/watch?v=HloGuAzP_H8&t=354s) helped me a lot.

## Cloudera quickstart VM and Java and Spark version ##

Cloudera quickstart comes with Spark 1.6.0 by default which forces you to get up to speed with [cloudera parcels](https://www.cloudera.com/documentation/enterprise/5-6-x/topics/cm_ig_parcels.html) about howto upgrade to Spark2.
Again following [tutorial video](https://www.youtube.com/watch?v=lQxlO3coMxM) helped me a lot.

## Acessing HBase table from Spark2 job ##

Okay I suppose you've got cloudera quickstart VM ready. Let's open the [hbase shell](https://www.tutorialspoint.com/hbase/hbase_shell.htm) and create simple table. Before going into it be familiar with basic concept of [Hbase](https://www.edureka.co/blog/hbase-architecture/).
Especially keep in mind:

```
Every HBase table must have at least one column family. Every piece of data in HBase is in a Cell (KeyValue) which has a row, column family, column qualifier, timestamp, and a value. The column families are defined statically 
when creating the table and cannot be altered without disabling the table. However, you can dynamically create any columns you want inside a column family - in fact each row could have it's own columns.
```

```sh
[cloudera@quickstart /]$ hbase shell
Java HotSpot(TM) 64-Bit Server VM warning: Using incremental CMS is deprecated and will likely be removed in a future release
19/05/22 13:19:52 INFO Configuration.deprecation: hadoop.native.lib is deprecated. Instead, use io.native.lib.available
HBase Shell; enter 'help<RETURN>' for list of supported commands.
Type "exit<RETURN>" to leave the HBase Shell
Version 1.2.0-cdh5.13.0, rUnknown, Wed Oct  4 11:16:18 PDT 2017

hbase(main):001:0> 
```

Now create **table "students"** with **one column family "data"** and three columns in that..name, surname, city:

```sh
hbase(main):003:0> create 'students', 'data'
0 row(s) in 2.3820 seconds

=> Hbase::Table - students
hbase(main):004:0> 
```

Columns are created in Hbase table when you insert the data...

```sh
hbase(main):013:0> put 'students', '1', 'data:name', 'Tomas'
0 row(s) in 0.0390 seconds

hbase(main):014:0> put 'students', '1', 'data:surname', 'Kloucek'
0 row(s) in 0.0100 seconds

hbase(main):015:0> put 'students', '1', 'data:city', 'Brno'
0 row(s) in 0.0120 seconds
```

Now to verify that data are saved in the HBase let's do the table scan:

```sh
hbase(main):016:0> scan 'students'
ROW                                          COLUMN+CELL                                                                                                                    
 1                                           column=data:city, timestamp=1558558916069, value=Brno                                                                          
 1                                           column=data:name, timestamp=1558558904451, value=Tomas                                                                         
 1                                           column=data:surname, timestamp=1558558911226, value=Kloucek                                                                    
1 row(s) in 0.0130 seconds
```

table ready let's do some coding....First we need to get HBaseConfiguration to access the HBase database. By using the hadoop configuration from the current Spark session 
you will automatically use cloudera platform configured HBase config files.

```java
     val spark = SparkSession
      .builder()
      .master("yarn")
      .getOrCreate()

     var hbConf = HBaseConfiguration.create(spark.sparkContext.hadoopConfiguration)
```
now let's put a new row into HBase table student:

```java
     var table = new HTable(hbConf, "students")

     // HBase PUT
     var put1 = new Put(Bytes.toBytes("2"));
     put1.addColumn(Bytes.toBytes("data"), Bytes.toBytes("name"), Bytes.toBytes("fromCodeName"));
     put1.addColumn(Bytes.toBytes("data"), Bytes.toBytes("surname"), Bytes.toBytes("fromCodeSurname"));
     put1.addColumn(Bytes.toBytes("data"), Bytes.toBytes("city"), Bytes.toBytes("fromCodeCity"));

     table.put(put1)
```

to verify that row has been inserted let's perform also scan from the code:

```java
    def printRow(result : Result) = {
      val cells = result.rawCells();
      print( Bytes.toString(result.getRow) + " : " )
      for(cell <- cells){
        val col_name = Bytes.toString(CellUtil.cloneQualifier(cell))
        val col_value = Bytes.toString(CellUtil.cloneValue(cell))
        print("(%s,%s) ".format(col_name, col_value))
      }
      println()
    }

     // Scanning the table...
     var scan = table.getScanner(new Scan())
     println("Reading the table students.")
     
     val result = scan.iterator

     while(result.hasNext)
     {
        val data = result.next 
        printRow(data)
     }

     scan.close()
     table.close()
```
I believe code is selfexplanatory. More important is to tweak the dependencies in the build.sbt:

```sh
  //SPARK
  "org.apache.spark" %% "spark-core" % "2.3.0" % Provided,
  "org.apache.spark" %% "spark-sql" % "2.3.0" % Provided,
  "org.apache.spark" %% "spark-hive" % "2.3.0" % Provided,
  "org.apache.spark" %% "spark-streaming" % "2.3.0" % Provided,
  
  //HBASE
  "org.apache.hbase" % "hbase-client" % "1.2.0-cdh5.13.0" % Provided,
  "org.apache.hbase" % "hbase-common" % "1.2.0-cdh5.13.0" % Provided,
```

First of all, the HBase libraries will be provided by the cloudera platform hence **"Provided" scope**. But to have this functional you need to have HBase libraries on Spark2 classpath. You have two options to do so:

* When you're going to be installing Spark 2 service through parcels, you will be asked whether you want HBase on classpath..Select that you do.
* There is also posibility to add hBase libraries also when submitting the spark job.

Okay code ready, let's prepare jar:

```sh
tomask79@utu:~/projects/apache-spark-2-scala-starter-template$ sbt package
[info] Loading project definition from /home/tomask79/projects/apache-spark-2-scala-starter-template/project
[info] Set current project to apache-spark-2-scala-starter-template (in build file:/home/tomask79/projects/apache-spark-2-scala-starter-template/)
[info] Updating {file:/home/tomask79/projects/apache-spark-2-scala-starter-template/}apache-spark-2-scala-starter-template...
[warn] Multiple dependencies with the same organization/name but different versions. To avoid conflict, pick one version:
[warn]  * org.scalatest:scalatest_2.11:(2.2.4, 2.2.6)
[info] Resolving jline#jline;2.12.1 ...
[info] Done updating.
[info] Compiling 2 Scala sources to /home/tomask79/projects/apache-spark-2-scala-starter-template/target/scala-2.11/classes...
[warn] there was one deprecation warning; re-run with -deprecation for details
[warn] one warning found
[info] Packaging /home/tomask79/projects/apache-spark-2-scala-starter-template/target/scala-2.11/apache-spark-2-scala-starter-template_2.11-1.0.jar ...
[info] Done packaging.
[success] Total time: 14 s, completed May 23, 2019 10:59:23 PM
tomask79@utu:~/projects/apache-spark-2-scala-starter-template$ 
```
## Submitting the HBase Spark 2 job to Cloudera cluster ##

Jar is ready but we need to **mount the directory with jar on the host to cloudera VM** first:

```sh
[root@quickstart Desktop]# mount -t vboxsf hbase-test /home/cloudera/workspace/host/
[root@quickstart Desktop]# exit
exit
[cloudera@quickstart Desktop]$ cd ~/workspace/host/
[cloudera@quickstart host]$ ls
apache-spark-2-scala-starter-template_2.11-1.0.jar  classes
[cloudera@quickstart host]$ 
```

jar is accessible in the cloudera VM so let's submit the Spark job under the cloudera user:

```sh
[cloudera@quickstart host]$ kinit cloudera
Password for cloudera@CLOUDERA: 
[cloudera@quickstart host]$ spark2-submit --class com.example.Main --deploy-mode client --master yarn apache-spark-2-scala-starter-template_2.11-1.0.ja
```

in the job output will see if you have Kerberos security enabled in your cloudera quickstart:

```java
Exception in thread "main" org.apache.hadoop.hbase.client.RetriesExhaustedWithDetailsException: Failed 1 action: org.apache.hadoop.hbase.security.AccessDeniedException: 
Insufficient permissions (user=cloudera@CLOUDERA, scope=default:students, family=data:name, params=[table=default:students,family=data:name],action=WRITE)
	at org.apache.hadoop.hbase.security.access.AccessController.prePut(AccessController.java:1651)
	at org.apache.hadoop.hbase.regionserver.RegionCoprocessorHost$30.call(RegionCoprocessorHost.java:918)
```

so let's give the cloudera user permission on the students table and submit the Spark 2 job again:

```sh
loudera@quickstart host]$ kinit hbase
Password for hbase@CLOUDERA: 
[cloudera@quickstart host]$ hbase shell
Java HotSpot(TM) 64-Bit Server VM warning: Using incremental CMS is deprecated and will likely be removed in a future release
19/05/23 14:37:22 INFO Configuration.deprecation: hadoop.native.lib is deprecated. Instead, use io.native.lib.available
HBase Shell; enter 'help<RETURN>' for list of supported commands.
Type "exit<RETURN>" to leave the HBase Shell
Version 1.2.0-cdh5.13.0, rUnknown, Wed Oct  4 11:16:18 PDT 2017

hbase(main):001:0> grant 'cloudera', 'RW', 'students'
0 row(s) in 1.4870 seconds

hbase(main):002:0> 
```

repeated submit should show following output:

```sh
[cloudera@quickstart host]$ spark2-submit --class com.example.Main --deploy-mode client --master yarn apache-spark-2-scala-starter-template_2.11-1.0.jar 
.
.
.
19/05/23 14:43:04 INFO zookeeper.ClientCnxn: Session establishment complete on server quickstart.cloudera/10.0.2.15:2181, sessionid = 0x16ae60b344001b4, negotiated timeout = 60000
Reading the table students.
1 : (city,Brno) (name,Tomas) (surname,Kloucek) 
2 : (city,fromCodeCity) (name,fromCodeName) (surname,fromCodeSurname) 
19/05/23 14:43:05 INFO client.ConnectionManager$HConnectionImplementation: Closing zookeeper sessionid=0x16ae60b344001b
```

That's it. Hope you found my Spark 2 Hbase demo usefull

Best Regards

Tomas
