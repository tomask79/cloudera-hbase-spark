package com.example

import org.apache.spark.{SparkContext, SparkConf}

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.client.{ConnectionFactory}
import org.apache.hadoop.hbase.{CellUtil, HBaseConfiguration, TableName}
import org.apache.hadoop.hbase.HBaseConfiguration
import org.apache.hadoop.hbase.client.{Result, Scan, HTable, Put}
import org.apache.hadoop.hbase.util.Bytes
import org.apache.spark.sql.{Row, SparkSession}

object HBaseTest {

  def printRow(result : Result) = {
    val cells = result.rawCells();
    print( Bytes.toString(result.getRow) + " : " )
    for(cell <- cells){
      val col_name = Bytes.toString(CellUtil.cloneQualifier(cell))
      val col_value = Bytes.toString(CellUtil.cloneValue(cell))
      print("(%s,%s) ".format(col_name, col_value))
    }
    println()
  }

  def runCode() = {
     val spark = SparkSession
      .builder()
      .master("yarn")
      .getOrCreate()

     var hbConf = HBaseConfiguration.create(spark.sparkContext.hadoopConfiguration)
     var table = new HTable(hbConf, "students")

     // HBase PUT
     var put1 = new Put(Bytes.toBytes("2"));
     put1.addColumn(Bytes.toBytes("data"), Bytes.toBytes("name"), Bytes.toBytes("fromCodeName"));
     put1.addColumn(Bytes.toBytes("data"), Bytes.toBytes("surname"), Bytes.toBytes("fromCodeSurname"));
     put1.addColumn(Bytes.toBytes("data"), Bytes.toBytes("city"), Bytes.toBytes("fromCodeCity"));

     table.put(put1)

     // Scanning the table...
     var scan = table.getScanner(new Scan())
     println("Reading the table students.")
     
     val result = scan.iterator

     while(result.hasNext)
     {
        val data = result.next 
        printRow(data)
     }

     scan.close()
     table.close()
  }
}
